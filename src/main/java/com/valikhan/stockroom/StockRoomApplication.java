package com.valikhan.stockroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockRoomApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockRoomApplication.class, args);
	}

}
