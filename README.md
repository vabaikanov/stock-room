# stock-room



## Getting started

Capstone project: Stock-Room web application
I have a warehouse of fast moving goods, which is represented by 100 line items to cover the 2 years demand. Employees can request the needed item and get in immediately, without waiting several months until procurement delivers the goods from abroad.
Currently, the information on the warehouse is managed manually, hence it is challenging to ensure visibility of the existing stock level, and maintain the accounting of transaction.
Below is the high level overview of the project idea:
 What kind of app would it be?
The proposed application is a web-based Materials Management System (MMS). This system will be designed to manage the inventory of a warehouse, allowing users to check the availability of items and request them. The system will also enable storekeepers to issue goods and update the stock levels accordingly.

Who is this project for?
This project is intended for my department that manage a warehouse or inventory of materials. The primary users of this system will include:

1.	Warehouse Staff/Admin: Responsible for issuing goods and updating stock levels.
2.	Registered Users: Individuals within the organization who need to request materials or goods from the warehouse. 
3.	Unregistered users must authorize first to see the warehouse data.
What needs will it satisfy?
1.	Inventory Management:
1.1.	Tracking Stock Levels: The system will keep real-time data on the quantity of each item in the warehouse.
1.2.	Stock Updates: Storekeepers can update the stock levels whenever items are issued or received.

2.	Request Management:
2.1.	User Requests: Employees can log in to the system, browse available items, and request the materials they need.
2.2.	Request Tracking: Users can track the status of their requests (pending, approved, issued).


3.	Efficiency and Accuracy:
3.1.	Minimize Errors: By automating the tracking and request process, the system reduces the chances of manual errors in inventory management.
3.2.	Faster Processing: Streamlined request and issuance process enhances efficiency, ensuring that users get the materials they need promptly.

4.	Reporting and Analytics:
4.1.	Usage Reports: Generate reports on item usage, helping in forecasting demand and managing inventory more effectively.
4.2.	Stock Alerts: Notifications for low stock levels to ensure timely reordering of materials.

5.	User Management:
5.1.	Access Control: Different levels of access for users, storekeepers, and managers to ensure data security and integrity.

